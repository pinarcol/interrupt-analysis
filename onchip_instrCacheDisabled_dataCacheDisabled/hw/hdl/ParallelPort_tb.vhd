library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity testbench is
	generic (
		N : natural := 3;
		L : natural := 8
	);
end testbench;

architecture bhv of testbench is
	component ParallelPort is
		port (
			Clk : in std_logic;
			nReset : in std_logic;
			Address : in std_logic_vector(N - 1 downto 0);
			ChipSelect : in std_logic;
			Read : in std_logic;
			Write : in std_logic;
			ReadData : out std_logic_vector (L - 1 downto 0);
			WriteData : in std_logic_vector (L - 1 downto 0);
			ParPort : inout std_logic_vector (7 downto 0)
		);
	end component;

	signal Clk : std_logic;
	signal nReset : std_logic;
	signal Address : std_logic_vector(N - 1 downto 0);
	signal ChipSelect : std_logic;
	signal Read : std_logic;
	signal Write : std_logic;
	signal nBE : std_logic_vector(3 downto 0);
	signal WaitRequest : std_logic;
	signal ReadData : std_logic_vector (L - 1 downto 0);
	signal SReadData : std_logic_vector (L - 1 downto 0);
	signal WriteData : std_logic_vector (L - 1 downto 0);
	signal ParPort : std_logic_vector (7 downto 0);

	constant CLK_PERIOD : time := 20 ns; --50 MHZ

begin
	UUT : ParallelPort
	port map(
		Clk => Clk, 
		nReset => nReset, 
		Address => Address(N - 1 downto 0), 
		ChipSelect => ChipSelect, 
		Read => Read, 
		Write => Write, 
		ReadData => ReadData, 
		WriteData => WriteData, 
		ParPort => ParPort
	);

	-- Generate CLK signal
	clk_generation : process
	begin
		CLK <= '1';
		wait for CLK_PERIOD / 2;
		CLK <= '0';
		wait for CLK_PERIOD / 2;
	end process clk_generation;
	
	simulation : process begin
		wait for CLK_PERIOD;

		-- Reset
		nReset <= '0';
		wait for CLK_PERIOD;
		nReset <= '1';
		wait for CLK_PERIOD;
		
		-- Write sth
		ChipSelect <= '1';
		
		Address <= "000";
		Write <= '1';
		WriteData <= X"0F";
		
		wait for CLK_PERIOD;
		
		Address <= "010";
		Write <= '1';
		WriteData <= X"0F";
		
		wait for CLK_PERIOD;
		
	end process;
end bhv;