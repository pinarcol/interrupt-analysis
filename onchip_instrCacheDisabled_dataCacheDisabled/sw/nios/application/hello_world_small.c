/* 
 * "Small Hello World" example. 
 * 
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example 
 * designs. It requires a STDOUT  device in your system's hardware. 
 *
 * The purpose of this example is to demonstrate the smallest possible Hello 
 * World application, using the Nios II HAL library.  The memory footprint
 * of this hosted application is ~332 bytes by default using the standard 
 * reference design.  For a more fully featured Hello World application
 * example, see the example titled "Hello World".
 *
 * The memory footprint of this example has been reduced by making the
 * following changes to the normal "Hello World" example.
 * Check in the Nios II Software Developers Manual for a more complete 
 * description.
 * 
 * In the SW Application project (small_hello_world):
 *
 *  - In the C/C++ Build page
 * 
 *    - Set the Optimization Level to -Os
 * 
 * In System Library project (small_hello_world_syslib):
 *  - In the C/C++ Build page
 * 
 *    - Set the Optimization Level to -Os
 * 
 *    - Define the preprocessor option ALT_NO_INSTRUCTION_EMULATION 
 *      This removes software exception handling, which means that you cannot 
 *      run code compiled for Nios II cpu with a hardware multiplier on a core 
 *      without a the multiply unit. Check the Nios II Software Developers 
 *      Manual for more details.
 *
 *  - In the System Library page:
 *    - Set Periodic system timer and Timestamp timer to none
 *      This prevents the automatic inclusion of the timer driver.
 *
 *    - Set Max file descriptors to 4
 *      This reduces the size of the file handle pool.
 *
 *    - Check Main function does not exit
 *    - Uncheck Clean exit (flush buffers)
 *      This removes the unneeded call to exit when main returns, since it
 *      won't.
 *
 *    - Check Don't use C++
 *      This builds without the C++ support code.
 *
 *    - Check Small C library
 *      This uses a reduced functionality C library, which lacks  
 *      support for buffering, file IO, floating point and getch(), etc. 
 *      Check the Nios II Software Developers Manual for a complete list.
 *
 *    - Check Reduced device drivers
 *      This uses reduced functionality drivers if they're available. For the
 *      standard design this means you get polled UART and JTAG UART drivers,
 *      no support for the LCD driver and you lose the ability to program 
 *      CFI compliant flash devices.
 *
 *    - Check Access device drivers directly
 *      This bypasses the device file system to access device drivers directly.
 *      This eliminates the space required for the device file system services.
 *      It also provides a HAL version of libc services that access the drivers
 *      directly, further reducing space. Only a limited number of libc
 *      functions are available in this configuration.
 *
 *    - Use ALT versions of stdio routines:
 *
 *           Function                  Description
 *        ===============  =====================================
 *        alt_printf       Only supports %s, %x, and %c ( < 1 Kbyte)
 *        alt_putstr       Smaller overhead than puts with direct drivers
 *                         Note this function doesn't add a newline.
 *        alt_putchar      Smaller overhead than putchar with direct drivers
 *        alt_getchar      Smaller overhead than getchar with direct drivers
 *
 */

#include "sys/alt_stdio.h"
#include "system.h"
#include "io.h"
#include "sys/alt_irq.h"
#include "altera_avalon_timer_regs.h"
#include "altera_avalon_timer.h"


#define IRESETVAL 0 //Change the values if your register map is different than
#define ICOUNTER 0
#define IRZ 1
#define ISTART 2
#define ISTOP 3
#define IIIRQEN 4
#define ICLREOT 5
#define RESETVAL 0XFF000000 //Counter starts counting from this value
#define IRQENVAL 1
#define IRQDISVAL 0
#define CLREOTVAL 1
#define ARBITVAL 0X0000FFFF



static void timer_isr(void* context);
static void counter_isr(void* context);
volatile static int snap = 0;
volatile static int flag = 0;

void test_counter()
{
	IOWR(COUNTER_0_BASE, IRESETVAL, RESETVAL);
	//Reset value is loaded
	IOWR(COUNTER_0_BASE, IRZ, ARBITVAL);
	//Reset activated to load the counter with the reset value
	alt_printf("iCounter after reset= %x\n",IORD(COUNTER_0_BASE, ICOUNTER));
	//Check that counter is loaded with the reset value
	IOWR(COUNTER_0_BASE, ISTART, ARBITVAL);
	//Start the counter
	alt_printf("iCounter after start= %x\n",IORD(COUNTER_0_BASE, ICOUNTER));
	//Read a value from the running counter
	IOWR(COUNTER_0_BASE, ISTOP, ARBITVAL);
	alt_printf("iCounter after stop1= %x\n",IORD(COUNTER_0_BASE, ICOUNTER));
	alt_printf("iCounter after stop2= %x\n",IORD(COUNTER_0_BASE, ICOUNTER));
	//Two consecutive reads to test that the counter is stopped. They should give the same result
	IOWR(COUNTER_0_BASE, ISTART, ARBITVAL);
	//Restart the counter
	alt_printf("iCounter after restart1=%x\n",IORD(COUNTER_0_BASE,ICOUNTER));
	alt_printf("iCounter after restart2=%x\n",IORD(COUNTER_0_BASE,ICOUNTER));
	//Two consecutive reads to test that the counter is stopped. They should give different results
	IOWR(COUNTER_0_BASE, ISTOP, ARBITVAL);
}


void test_interrupt_timer() {
	alt_ic_isr_register(TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID,TIMER_0_IRQ, timer_isr, NULL,NULL);
	flag=0 ; //Flag is a global variable
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,0); //Clear control register
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,2); //Continuous mode ON
	IOWR_ALTERA_AVALON_TIMER_PERIODL(TIMER_0_BASE, 0xFFFF); //Set initial value
	IOWR_ALTERA_AVALON_TIMER_PERIODH(TIMER_0_BASE, 0x00FF);
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,3); //Enable timer interrupt
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,7); //Start timer
	while(1)
	{
	// Normal program routine HERE…
	 if(flag)
	 {
	 alt_printf("%x \n",0xffff-snap+1);
	 flag=0;
	 IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,7); //Enable IRQ and Start timer
	 }
	}
}

void test_interrupt_counter() {
	alt_ic_isr_register(COUNTER_0_IRQ_INTERRUPT_CONTROLLER_ID,COUNTER_0_IRQ,counter_isr, NULL,NULL);
	flag=0 ; //Flag is a global variable
	IOWR(COUNTER_0_BASE, IRESETVAL, RESETVAL); //Reset value is loaded
	IOWR(COUNTER_0_BASE, IRZ, ARBITVAL); //Reset activated to load the counter with the reset value
	IOWR(COUNTER_0_BASE, IIIRQEN, IRQENVAL); //Enable IRQ
	IOWR(COUNTER_0_BASE, ISTART, ARBITVAL); //Start the counter

	while(1)
	{
	// Normal program routine HERE…
	if(flag)
	 {
	 alt_printf("%x \n",snap);
	 flag=0;
	 IOWR(COUNTER_0_BASE, IIIRQEN, IRQENVAL); //Enable the interrupt
	 IOWR(COUNTER_0_BASE, ISTART, ARBITVAL); //Start the counter
	 }
	};
}

static void timer_isr(void* context) {
	 IOWR_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE, ARBITVAL);
	 snap = IORD_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE);
	 IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,0); //Clear interrupt (ITO)
	 IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE, 0); //CLEAR TO
	 flag=1; //Flag is a global variable
}

static void counter_isr(void* context)
{
 snap = IORD(COUNTER_0_BASE, ICOUNTER);
 IOWR(COUNTER_0_BASE, IIIRQEN, IRQDISVAL); //Clear interrupt
 IOWR(COUNTER_0_BASE, ICLREOT, CLREOTVAL); //Clear iEOT
 IOWR(COUNTER_0_BASE, ISTOP, ARBITVAL); //Stop the counter
 IOWR(COUNTER_0_BASE, IRZ, ARBITVAL); //Reset the counter
 flag=1; //Flag is a global variable
}

int main()
{ 
	// alt_ic_isr_register(TIMER_0_IRQ_INTERRUPT_CONTROLLER_ID, TIMER_0_IRQ, timer_isr, NULL, NULL);
	// alt_ic_isr_register(0, 0, counter_isr, NULL, NULL);
	alt_ic_isr_register(COUNTER_0_IRQ_INTERRUPT_CONTROLLER_ID, COUNTER_0_IRQ, counter_isr, NULL, NULL);

	alt_putstr("Hello from Nios II!\n");
	test_counter();
	//test_interrupt_timer();
	test_interrupt_counter();
	alt_putstr("End\n");

	/* Event loop never exits. */
	while (1);

	return 0;
}
